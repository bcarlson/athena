#include "MuonCondTool/MuonAlignmentDbTool.h"
#include "MuonCondTool/MDT_DCSConditionsTool.h"
#include "MuonCondTool/MDT_DCSConditionsRun2Tool.h"
#include "MuonCondTool/RpcDetectorStatusDbTool.h"
#include "MuonCondTool/MDT_DeadTubeConditionsTool.h"
#include "MuonCondTool/TGC_STATUSConditionsTool.h"
#include "MuonCondTool/MDT_MapConversion.h"
#include "MuonCondTool/CSC_DCSConditionsTool.h"
#include "MuonCondTool/RPC_DCSConditionsTool.h"
#include "MuonCondTool/MDT_DQConditionsTool.h"
#include "MuonCondTool/MuonAlignmentErrorDbTool.h"

DECLARE_COMPONENT( MuonAlignmentDbTool )
DECLARE_COMPONENT( MDT_DCSConditionsTool )
DECLARE_COMPONENT( CSC_DCSConditionsTool )
DECLARE_COMPONENT( RPC_DCSConditionsTool )
DECLARE_COMPONENT( TGC_STATUSConditionsTool )
DECLARE_COMPONENT( RpcDetectorStatusDbTool )
DECLARE_COMPONENT( MDT_DeadTubeConditionsTool )
DECLARE_COMPONENT( MDT_DQConditionsTool )
DECLARE_COMPONENT( MDT_MapConversion )
DECLARE_COMPONENT( MuonAlignmentErrorDbTool )
DECLARE_COMPONENT( MDT_DCSConditionsRun2Tool )

